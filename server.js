var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path =require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/clientes/:idcliente',function(req,res){
//res.sendFile(path.join(__dirname, 'clientes_param.json'))
res.sendFile(path.join(__dirname, 'cliente' + req.params.idcliente + '.json'));
});

app.get('/',function(req,res){
res.sendFile(path.join(__dirname, 'clientes.json'))

});

app.post('/',function(req,res){
  res.sendFile(path.join(__dirname, 'post.json'))
});

app.put('/',function(req,res){
res.sendFile(path.join(__dirname, 'clientes.json'))
});

app.delete('/',function(req,res){
  res.sendFile(path.join(__dirname, 'clientes.json'))
});
